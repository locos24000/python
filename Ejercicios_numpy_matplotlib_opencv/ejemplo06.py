import cv2
import tkinter as tk
from PIL import Image, ImageTk
import numpy as np
import os
def invertir_escala_grises(valor):
    valor_float = float(valor)
    img_invertida = (255 - imagen_original.astype(np.uint8)) * valor_float
    img_pil = Image.fromarray(img_invertida.astype(np.uint8))
    img_tk = ImageTk.PhotoImage(img_pil)
    canvas.itemconfig(canvas_img, image=img_tk)
    canvas.img_tk = img_tk

def cambiar_color_rojo(valor):
    img_cambiada = imagen_original.astype(np.int16)
    img_cambiada[..., 2] = np.clip(img_cambiada[..., 2] + int(valor), 0, 255).astype(np.uint8)
    img_pil = Image.fromarray(img_cambiada)
    img_tk = ImageTk.PhotoImage(img_pil)
    canvas.itemconfig(canvas_img, image=img_tk)
    canvas.img_tk = img_tk

ventana = tk.Tk()
imagen_original =  cv2.imread(os.path.dirname(__file__)+'\\santacruz.jpg')
scale = tk.Scale(ventana, from_=0, to=1, resolution=0.1, orient=tk.HORIZONTAL,
                 command=invertir_escala_grises)
scale.pack()

scale_rojo = tk.Scale(ventana, from_=0, to=255, orient=tk.HORIZONTAL, command=cambiar_color_rojo)
scale_rojo.pack()

canvas = tk.Canvas(ventana, width=400, height=300)
canvas.pack()
img_pil = Image.fromarray(imagen_original)
img_tk = ImageTk.PhotoImage(img_pil)
canvas_img = canvas.create_image(0, 0, anchor=tk.NW, image=img_tk)
canvas.img_tk = img_tk
ventana.mainloop()  