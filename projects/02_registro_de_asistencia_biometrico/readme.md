# Juego: Galaxy wars 

Galaxy wars es un juego echo en python que tienes que evitar que las naves bajen demasiado 

## Descripción del Proyecto
un emocionante y desafiante videojuego de disparos en 2D. El objetivo del juego es controlar una nave y eliminar a otras naves enemigas mientras esquiva obstáculos y dispara proyectiles. del abecedario, con un límite de 7 intentos por palabra.

## Configuración y Ejecución del Proyecto

Para configurar y ejecutar el juego, sigue los siguientes pasos:

- Clona este repositorio en tu sistema local.
- Asegúrate de tener Python 3.11 instalado.
- Ejecuta el archivo galaxy wars.py para iniciar el juego
## Librerías Utilizadas

- pygame 

Es una biblioteca de código abierto para Python que se utiliza principalmente para desarrollar videojuegos y aplicaciones multimedia interactivas. Proporciona una amplia gama de herramientas y funciones para trabajar con gráficos, sonido, eventos de entrada y otras características esenciales para el desarrollo de juegos. 


## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado "Programa y Libera tu Potencial" ha desempeñado un papel fundamental como nuestra guía principal, brindándonos una base sólida en la comprensión de los elementos esenciales de los algoritmos y la programación en Python. Su enfoque en la potenciación de la creatividad a través de la codificación ha sido un pilar esencial en la concepción y desarrollo de este proyecto.

![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)
## Agradecimientos

Deseamos expresar nuestro más sincero agradecimiento al docente, al decano de la facultad de ingeniería ya la Universidad Privada Domingo Savio - Sede (Santa Cruz) por su invaluable apoyo y orientación a lo largo de este proyecto. Su compromiso y respaldo han sido fundamentales para el éxito de nuestro trabajo..
## Cómo Contribuir
Como proyecto universitario, las contribuciones están limitadas a los miembros del equipo. Sin embargo, si encuentras algún error o tienes alguna sugerencia para mejorar el código o los análisis, no dudes en contactarte con el docente o el equipo de desarrollo.

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UNIVERSIDAD PRIVADA DOMINGO SAVIO](https://www.facebook.com/UPDS.bo)

Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)
Docente:
- [PhD. Jaime Zambrana Chacón](https://facebook.com/zambranachaconjaime)

Equipo de desarrollo:
- Michael Abraham Castro Torrez
- Juan Pablo Muiba Triguero
- Pablo Moreno Terrazas
- Pablo Antonio Mamani Gonzales