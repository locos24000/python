(deftemplate respuesta-si-no
    (slot respuesta
        (type SYMBOL)
        (allowed-symbols si no ninguno)
        (default ninguno)
    )
)
(deffacts respuesta-si-no
	(respuesta no)
	(respuesta si)	
)