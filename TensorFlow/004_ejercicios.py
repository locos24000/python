# Jaime Zambrana Chacón.
# Mis redes sociales
# https://www.youtube.com/tecnoprofe
# https://www.facebook.com/zambranachaconjaime
# https://gitlab.com/tecnoprofe
# https://www.tiktok.com/@jaimezambranachacon?lang=es
# <<<=============================================>>>
# <<<========= Multiplicación de Matrices ========>>>
import numpy as np
A=np.array([[2,1],[3,9]])
print("Matriz A")
print(A)
B=np.array([[1,4],[8,2]])
print("Matriz B")
print(B)
print("___A*B____")
print(np.matmul(A,B))