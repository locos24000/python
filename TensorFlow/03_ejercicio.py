# Ejercicio 3.
#Convierte el siguiente tensor en un objeto tensor de reales con 2 fila y 3 columnas: ([1, 2, 3, 4,5, 6])
import math
from numpy.core.numeric import indices
import tensorflow as tf
A=tf.Variable([1,2,3,4,5,6],dtype=tf.float32)
B=tf.reshape(A,shape=(2,3))
print(B)