import math
class Ecuacion2grado:
    a=None
    b=None
    c=None
    x1=None
    x2=None
    d=None
    
    def __init__(self,a,b,c):
        self.a=a
        self.b=b
        self.c=c

    def solucion(self):
        self.d=(self.b*self.b)-4*self.a*self.c
        if(self.d>=0):
            self.x1=(-self.b + math.sqrt(self.d))/(2*self.a)
            self.x2=(-self.b - math.sqrt(self.d))/(2*self.a)
            #print("x1=",self.x1," x2=",self.x2)
        else:
            return "Solucion imaginaria"   

# x=Ecuacion2grado()

# a=int(input("introduzca a "))
# b=int(input("introduzca b "))
# c=int(input("introduzca c "))
# ecu=Ecuacion2grado(a,b,c)
# ecu.solucion()