class Animal:
    nombre = None
    tipo_de_alimentacion = None
    tipo_de_reproduciion = None

class Vertebrado(Animal):
    tipo_de_piel = None    

class Invertebrado(Animal):
    tipo_de_patas = None
    #8 patas, 4 patas, sin patas

class SangreFria(Vertebrado):
    tipo_de_reptil = None
    #nacuatico, terrestre, volador

class Pez(Vertebrado):
    cantidad_de_aletas = None
    #3 aletas, 4 aletas, 5 aletas.

class Ave(Vertebrado):
    tipo_de_ave = None
    #ave de rapiña, voladora, no voladora.

class Anfibios(Vertebrado):
    tipo_de_anfibio = None
    #venenoso y no venenoso

class Reptil(Vertebrado):
    Tipo_de_reptil = None
    #con escamas, piel, piel mucosa.

class Acuatico(Invertebrado):
    tipo_de_reptil = None
    #tinta, puas, veneno, electricidad.

class Terrestre(Invertebrado):
    tipo_de_desplazamiento = None
    #arrastran, vuelan, caminan.

class Insecto(Invertebrado):
    tipo_de_insecto = None
    #servicial, perjudicial.

class Equinodermo(Invertebrado):
    tipo_de_equinodermo = None
    #estrellas de mar, erizos de mar.

class Gusano(Invertebrado):
    Lugar_de_residencia = None
    #gusano de tierra, frutas, verduras, intestinales.

class Celentero(Invertebrado):
    tipo_de_vivencia = None
    #sedentarios, trasladan.

class Esponja(Invertebrado):
    tipo_de_espomja = None
    #

class Aracnido(Invertebrado):
    tipo_de_aracnido = None
    #escorpiones, arañas

class Crustaceo(Invertebrado):
    tipo_de_transporte = None
    #saln a superficie, caminan, nadan, sedentarios.

class Mamifero(Vertebrado):
    tipo_de_desplazamiento = None
    tipo_de_postura = None
    #caminar, saltar, arrastrarse, volar, nadar
    #biped
        print("miau")

    def detalles(self):
        print('Nombre: ',self.nombre)
        print('Tipo: ',self.tipo_de_postura)
        print('Desplazamiento',self.tipo_de_desplazamiento)

michi = Gato()
michi.maullar()


class Gallina(Ave):    
    def cacarear(self):
        print("kokoroko")

    def detalles(self):
        print('Nombre: ',self.nombre)
        print('Tipo: ',self.tipo_de_ave)


turuleka = Gallina()
turuleka.nombre="Turuleka"
turuleka.tipo_de_ave="No voladora"
turuleka.tipo_de_piel="Plumas"
turuleka.tipo_de_reproduciion="ovipara"
turuleka.cacarear()
turuleka.detalles()
