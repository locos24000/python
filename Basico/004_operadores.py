# OPERADOR +=
r = 5
r += 10
print("el resultado es: ",r)
            # da como resultado 15

# OPERADOR -=
r = 5
r -= 10
print("el resultado es: ",r)
            # da como resultado -5

# OPERADOR *=
r = 5
r *= 10
print("el resultado es: ",r)
            # da como resultado 50             

# OPERADOR /=
r = 10
r /= 3
print("el resultado es: ",r)
            # da como resultado 3.3333

# OPERADOR **=
r = 2
r **= 3 # Es equivalente a 2^3
print("el resultado es: ",r)
            # da como resultado 8            

# OPERADOR //= 
r = 10
r //= 3 # Es una división entera
print("el resultado es: ",r)
            # da como resultado 3, por que es una división
            # que solo obtiene la parte entera
             
# OPERADOR %= 
r = 10
r %= 3 # obtiene el resto de la división
print("el resultado es: ",r)
            # da como resultado 1

# OPERADOR %= 
r = 9
r %= 3 # obtiene el resto de la división
print("el resultado es: ",r)
            # En este caso devuelve 0
