import matplotlib.pyplot as plt
import pandas
from collections import Counter
import os

#print (os.path.dirname(__file__)+"\\ejer_pandas.svg")
datos=pandas.read_csv(os.path.dirname(__file__)+"\\tiendapcok.csv")
print("______ cabecera___ ")
print(datos.head(3))
print("______ Shape___ ")
print(datos.shape)
print("______ promedio ___ ")
print("Promedio",datos['precio'].mean())
print("______ datos por columna ___ ")
print(datos[['precio','marca']])
print("______ Describe ___ ")
print(datos.describe())