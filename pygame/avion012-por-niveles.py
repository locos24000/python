from random import random
import pygame
import os
import random

ANCHO = 1024
ALTO = 768
DIMENSIONES=(ANCHO,ALTO)
NEGRO = (0, 0, 0)
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES,pygame.FULLSCREEN)
pygame.mixer.init()
sonido_fondo = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\fondo4.mp3")
pygame.mixer.Sound.play(sonido_fondo)
ventana1 = pygame.display.set_mode(DIMENSIONES,pygame.FULLSCREEN)
pygame.display.set_caption("Aviones")
clock = pygame.time.Clock()
bala = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\laser02.mp3")
choque_nave = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\golpe111.mp3")
explosion = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\golpe1.mp3")
fuente = pygame.font.Font('freesansbold.ttf', 32)
class Nave(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave4.png")
        self.image.set_colorkey(NEGRO)
        self.rect = self.image.get_rect()
        self.rect.centerx = ANCHO // 2
        self.rect.bottom = ALTO - 100
        self.speed_x = 0
        self.vida=30
        self.puntos=0

    def update(self):
        self.speed_x = 0
        self.speed_y = 0
        keystate=pygame.key.get_pressed()
        
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave4.png")
        if keystate[pygame.K_LEFT]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave5.png")
            self.speed_x = -3
        if keystate[pygame.K_RIGHT]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave5.png")
            self.speed_x = 3 
        if keystate[pygame.K_UP]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave5.png")
            self.speed_y = -3
        if keystate[pygame.K_DOWN]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave4.png")
            self.speed_y = 3  

        self.rect.x += self.speed_x        
        self.rect.y += self.speed_y
    
        if self.rect.right > ANCHO:
            self.rect.right = ANCHO
        if self.rect.left < 0:
            self.rect.left = 0
        
        if self.rect.bottom > ALTO:
            self.rect.bottom = ALTO
        if self.rect.top < 0:
            self.rect.top = 0


    def disparar(self):
        bala = Bala(self.rect.centerx, self.rect.top)
        all_sprites.add(bala)
        balas.add(bala)

class Bala(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\laser5.png")
        self.image.set_colorkey(NEGRO)
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.centerx = x
        self.speedy = -10

    def update(self):
        self.rect.y += self.speedy
        if self.rect.bottom < 0:
            self.kill()

fotos_meteoritos=[
    os.path.dirname(__file__)+"\\img\\nave\\meteorito19.png",
    os.path.dirname(__file__)+"\\img\\nave\\meteorito18.png",
    os.path.dirname(__file__)+"\\img\\nave\\meteorito17.png",
    os.path.dirname(__file__)+"\\img\\nave\\meteorito16.png",
    os.path.dirname(__file__)+"\\img\\nave\\meteorito15.png",
    os.path.dirname(__file__)+"\\img\\nave\\meteorito14.png",
]

class Meteorito(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(fotos_meteoritos[random.randint(0,5)])
        self.image.set_colorkey((0,0,0))
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(ANCHO - self.rect.width)
        self.rect.y = random.randrange(-150,-50)
        self.velocidad_y = random.randrange(1, 6)
        self.velocidad_x = random.randrange(-6, 6)

    def update(self):
        self.rect.x += self.velocidad_x
        self.rect.y += self.velocidad_y
        if self.rect.top > ALTO + 10 or self.rect.left < -25 or self.rect.right > ANCHO + 22 :
            self.rect.x = random.randrange(ANCHO - self.rect.width)
            self.rect.y = random.randrange(-150, -50)
            self.velocidad_y = random.randrange(1, 4)
            self.velocidad_x = random.randrange(-4, 4)
         
def salir():    
    fuente = pygame.font.Font('freesansbold.ttf', 32)    
    texto_salir = fuente.render("Presione la tecla 'a' para salir!!!", True, (195,157,154),(0,0,45))        
    while True:                   
        for evento in pygame.event.get():                         
            if evento.type == pygame.KEYDOWN:                 
                if evento.key == pygame.K_a:                     
                    quit()     
                else:
                    return 0                
        ventana.fill((NEGRO))        
        ventana.blit(texto_salir,(ANCHO//4,ALTO//2))
        pygame.display.flip()
def nivel2():           
    while True:                   
        for evento in pygame.event.get():                         
            if evento.type == pygame.KEYDOWN:                 
                if evento.key == pygame.K_RETURN:                     
                    print ("2")
                    pygame.init()
                    ventana = pygame.display.set_mode(DIMENSIONES,pygame.FULLSCREEN)
                    pygame.display.set_caption("Aviones")
                    clock = pygame.time.Clock()  
                    bala = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\laser02.mp3")
                    choque_nave = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\golpe111.mp3")
                    explosion = pygame.mixer.Sound(os.path.dirname(__file__)+"\\sonido\\golpe1.mp3")

                    fuente = pygame.font.Font('freesansbold.ttf', 32) 
                    class Nave(pygame.sprite.Sprite):
                     def __init__(self):
                        super().__init__()
                        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1.png")
                        self.image.set_colorkey(NEGRO)
                        self.rect = self.image.get_rect()
                        self.rect.centerx = ANCHO // 2
                        self.rect.bottom = ALTO - 100
                        self.speed_x = 0
                        self.vida=50
                        self.puntos=0

                     def update(self):
                       self.speed_x = 0
                       self.speed_y = 0
                       keystate=pygame.key.get_pressed()
        
                       self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1.png")
                       if keystate[pygame.K_LEFT]:
                          self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave2.png")
                          self.speed_x = -3
                       if keystate[pygame.K_RIGHT]:
                          self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave2.png")
                          self.speed_x = 3 
                       if keystate[pygame.K_UP]:
                          self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave2.png")
                          self.speed_y = -3
                       if keystate[pygame.K_DOWN]:
                          self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1.png")
                          self.speed_y = 3  

                       self.rect.x += self.speed_x        
                       self.rect.y += self.speed_y

                       if self.rect.right > ANCHO:
                        self.rect.right = ANCHO
                       if self.rect.left < 0:
                        self.rect.left = 0

                       if self.rect.bottom > ALTO:
                        self.rect.bottom = ALTO
                       if self.rect.top < 0:
                        self.rect.top = 0


                     def disparar(self):
                         bala = Bala(self.rect.centerx, self.rect.top-20)
                         bala1 = Bala(self.rect.centerx+32, self.rect.top+10)     
                         bala2 = Bala(self.rect.centerx-32, self.rect.top+10)
                         todos_los_sprites.add(bala,bala1,bala2)
                         balas.add(bala,bala1,bala2)

                    class Bala(pygame.sprite.Sprite):
                     def __init__(self, x, y):
                      super().__init__()
                      self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\laser6.png")
                      self.image.set_colorkey(NEGRO)
                      self.rect = self.image.get_rect()
                      self.rect.y = y
                      self.rect.centerx = x
                      self.speedy = -10

                     def update(self):
                      self.rect.y += self.speedy
                      if self.rect.bottom < 0:
                        self.kill()

                    fotos_meteoritos=[
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito22.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito21.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito20.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito19.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito18.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito17.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito16.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito15.png",
                      os.path.dirname(__file__)+"\\img\\nave\\meteorito14.png",
                    ]

                    class Meteorito(pygame.sprite.Sprite):
                     def __init__(self):
                      super().__init__()
                      self.image = pygame.image.load(fotos_meteoritos[random.randint(0,8)])
                      self.image.set_colorkey((0,0,0))
                      self.rect = self.image.get_rect()
                      self.rect.x = random.randrange(ANCHO - self.rect.width)
                      self.rect.y = random.randrange(-150,-50)
                      self.velocidad_y = random.randrange(1,8)
                      self.velocidad_x = random.randrange(-8, 8)

                     def update(self):
                      self.rect.x += self.velocidad_x
                      self.rect.y += self.velocidad_y
                      if self.rect.top > ALTO + 10 or self.rect.left < -25 or self.rect.right > ANCHO + 22 :
                       self.rect.x = random.randrange(ANCHO - self.rect.width)
                       self.rect.y = random.randrange(-150, -50)
                       self.velocidad_y = random.randrange(1, 4)
                       self.velocidad_x = random.randrange(-4, 4)
                    def salir():    
                     fuente = pygame.font.Font('freesansbold.ttf', 32)    
                     texto_salir = fuente.render("Presione la tecla 'a' para salir!!!", True, (195,157,154),(0,0,45))        
                     while True:                   
                       for evento in pygame.event.get():                         
                        if evento.type == pygame.KEYDOWN:                 
                         if evento.key == pygame.K_a:                     
                           quit()     
                         else:
                            return 0                
                       ventana.fill((NEGRO))        
                       ventana.blit(texto_salir,(ANCHO//4,ALTO//2))
                       pygame.display.flip()

                    fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo11.png")
                    todos_los_sprites = pygame.sprite.Group()
                    balas = pygame.sprite.Group()
                    meteoritos = pygame.sprite.Group()

                    avion = Nave()
                    todos_los_sprites.add(avion)
                    bala1 = Nave()
                    todos_los_sprites.add(bala1)

                    for i in range(10):
                        meteorito=Meteorito()
                        todos_los_sprites.add(meteorito)
                        meteoritos.add(meteorito)

                    while True:
                     clock.tick(300)
                     for evento in pygame.event.get():                         
                       if evento.type == pygame.KEYDOWN: 
                         if evento.key == pygame.K_ESCAPE: 
                            salir()        
                         if evento.key == pygame.K_SPACE: 
                           pygame.mixer.Sound.play(bala)
                           avion.disparar() 

                     todos_los_sprites.update()
                     ventana.blit(fondo,(0,0))

                     choques = pygame.sprite.groupcollide(meteoritos,balas, True, True)            
                     for choque in choques:
                       pygame.mixer.Sound.play(explosion)
                       bala1.puntos +=1                  
                       meteorito = Meteorito()
                       todos_los_sprites.add(meteorito)
                       meteoritos.add(meteorito)
                       if bala1.puntos>= 300:
                        fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo03.png")
                        if bala1.puntos> 300:
                         quit()
                     choques = pygame.sprite.spritecollide(avion,meteoritos, True) 
                     for choque in choques:
                       pygame.mixer.Sound.play(choque_nave)
                       avion.vida -= 1
                       meteorito = Meteorito()
                       todos_los_sprites.add(meteorito)
                       meteoritos.add(meteorito)
                       if avion.vida<= 0:
                        fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo02.png")
                        if avion.vida< 0:
                         quit()
                     texto_vida = fuente.render('Vida= '+str(avion.vida), True, (0,255,0))
                     ventana.blit(texto_vida,(0,0))  
                     texto_puntos =fuente.render('Puntos(300)='+str(bala1.puntos), True,  (200,100,0))
                     ventana.blit(texto_puntos,(700,0)) 
                     texto_nivel =fuente.render('NIVEL 2 ',True,(200,100,210))
                     ventana.blit(texto_nivel,(420,0)) 
                     todos_los_sprites.draw(ventana)    
                     pygame.display.flip()
                else:
                    return 0                
            ventana1.fill((158,185,65))        
            pygame.display.flip()
fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo12.png")
all_sprites = pygame.sprite.Group()
balas = pygame.sprite.Group()
meteoritos = pygame.sprite.Group()
avion = Nave()
all_sprites.add(avion)
bala1 = Nave()
all_sprites.add(bala1)
for i in range(10):
    meteorito=Meteorito()
    all_sprites.add(meteorito)
    meteoritos.add(meteorito)
while True:   
    clock.tick(120)
    for evento in pygame.event.get():                         
        if evento.type == pygame.KEYDOWN: 
            if evento.key == pygame.K_ESCAPE: 
                salir()        
            if evento.key == pygame.K_SPACE:                                         
                pygame.mixer.Sound.play(bala)
                avion.disparar() 
  
    all_sprites.update()
    ventana.blit(fondo,(0,0))
    choques = pygame.sprite.groupcollide(meteoritos,balas, True, True)  
        
    for choque in choques:
        pygame.mixer.Sound.play(explosion)
        bala1.puntos +=1               
        meteorito = Meteorito()
        all_sprites.add(meteorito)
        meteoritos.add(meteorito)
      
        if bala1.puntos>= 100:
            fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo01.png")
            nivel2()
    choques = pygame.sprite.spritecollide(avion,meteoritos, True) 
    for choque in choques:
        pygame.mixer.Sound.play(choque_nave)
        avion.vida -= 1
        meteorito = Meteorito()
        all_sprites.add(meteorito) 
        meteoritos.add(meteorito)
        if avion.vida<= 0:
            fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo02.png")
            if avion.vida< 0:
             quit()
             
    texto_vida =fuente.render('Vida= '+str(avion.vida), True, (0,200,0))
    ventana.blit(texto_vida,(0,0))  
    texto_puntos =fuente.render('Puntos(100)='+str(bala1.puntos), True,  (200,100,0))
    ventana.blit(texto_puntos,(700,0)) 
    texto_nivel =fuente.render('NIVEL 1 ',True,(200,100,210))
    ventana.blit(texto_nivel,(420,0)) 
    all_sprites.draw(ventana)    
    pygame.display.flip()