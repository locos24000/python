import pygame
pygame.init()
ventana = pygame.display.set_mode((300, 300)) 
clock = pygame.time.Clock() 
FPS = 100 # Frames por segundo
NEGRO = (0, 0, 0) 
BLANCO = (255, 255, 255)
ROJO = (100,0,0)
AZUL = (0,0,100)
VERDE=(0,100,0)
AMARILLO =(247,247,0)
ancho,alto=50,150
rectangulo = pygame.Rect((100, 150), (ancho,alto)) 
image = pygame.Surface((ancho,alto)) 
image.fill(AZUL)
while True:
    clock.tick(FPS)
    for evento in pygame.event.get(): 
        if evento.type == pygame.QUIT: 
            quit()
        elif evento.type == pygame.KEYDOWN: 
            if evento.key == pygame.K_UP: 
                rectangulo.move_ip(0, -50)
            elif evento.key == pygame.K_DOWN: 
                rectangulo.move_ip(0, 50)
            elif evento.key == pygame.K_LEFT: 
                rectangulo.move_ip(-50, 0)
            elif evento.key == pygame.K_RIGHT: 
                rectangulo.move_ip(50, 0)
    ventana.fill(AMARILLO)
    ventana.blit(image, rectangulo) 
    pygame.display.update()