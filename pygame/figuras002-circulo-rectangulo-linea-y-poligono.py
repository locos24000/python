# Importar la librería de Juegos
from cmath import pi
import pygame
# Inicia la libreía.
pygame.init()
# Crear una ventana de 1024X764
ventana=pygame.display.set_mode((1024,700))
# Reloj de sistema desde pygame
reloj=pygame.time.Clock()
# velocidad de fps
velocidad=30
x=100
y=45
# ========================================
# Bucle principal

while True:
    # mover 1 fotograma en un segundo
    reloj.tick(velocidad)
    # Bucle para pedir eventos    		
    for evento in pygame.event.get():        
        # Compara si se presionó el boton X de la ventana
        if evento.type==pygame.QUIT:                
            # Salir
            quit()  

    #========================
    # LOGICA DEL PROGRAMA

    x+=10
    y+=1

    # =======================
    # PINTADO Y DIBUJADO
    # Pintar de color (R,G,B) El fondo
    ventana.fill((0,80,150))

    # Dibuja un rectangulo                 (x,  y, ancho,alto)
    pygame.draw.rect(ventana,(255,255,255),(x,200,200,100))

    # Dibuja un rectangulo                 (x,  y, ancho,alto)
    pygame.draw.rect(ventana,(200,100,50),(180,10,50,50))

    # Dibuja una linea                 (x1,y1)  , (x2,y2), ancho
    pygame.draw.line(ventana,(0,0,255),(100,y),(600,300),7)
    # Dibuja un poligono                      [(x1,y1),(x2,y2),(x3,y3),...(xn,yn)], ancho
    pygame.draw.polygon(ventana,(255,100,100),[(x,512),(350,70),(0,512)],10)
    
    # Dibuja un circulo                     (x,y), radio
    pygame.draw.circle(ventana,(255,255,0),(100,y),20)

                #arc(base, color        ,  x1, y1, ancho,alto  , start_angle, stop_angle) -> Rect
    pygame.draw.arc(ventana, (255,255,0), [200,200,500,500], pi, pi/90)
        
    # Actualiza la ventana, constantemente...
    pygame.display.update()