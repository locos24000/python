# EJERCICIO 1. Escribir un programa que permita al usuario ingresar 
# dos años y luego imprima todos los años en ese rango, 
# que sean bisiestos y múltiplos de 10. 
# Nota: para que un año sea bisiesto debe ser divisible por 4
# y no debe ser divisible por 100, 
# excepto que también sea divisible por 400.



# EJERCICIO 2.Escriba un programa que pregunte cuántos números se van a introducir,
# pida esos números, y escriba el mayor, el menor y la media aritmética.
# Se recuerda que la media aritmética de un conjunto de valores 
# es la suma de esos valores dividida por la cantidad de valores.
# Cuantos numeros digitara
n=int(input("Cuantos numeros digitara: "))
Lista1=[]
mayor = 0
menor = 0
suma = 0
for i in range(0,n):
    Lista1.append(int(input("Digite los numeros: ")))
mayor = max(Lista1)
menor = min(Lista1)
for i in Lista1:
    suma += i
#sacamos la media 
media = suma/n
#Imprimimos media 
print("el numero mayor es"+" "+str(mayor)+" "+"el numero menor es"+" "+str(menor)+" "+"la media es"+" "+str(media))